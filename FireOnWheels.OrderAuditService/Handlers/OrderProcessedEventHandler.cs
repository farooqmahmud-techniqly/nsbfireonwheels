﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FireOnWheels.QueueService.Common.Messages;
using NServiceBus;
using Serilog;

namespace FireOnWheels.OrderAuditService.Handlers
{
    public class OrderProcessedEventHandler : IHandleMessages<IOrderProcessedEvent>
    {
        private readonly IBus _bus;
        private readonly ILogger _logger;

        public OrderProcessedEventHandler(IBus bus, ILogger logger)
        {
            _bus = bus;
            _logger = logger.ForContext(GetType());
        }
        
        public void Handle(IOrderProcessedEvent message)
        {
            _logger.Debug("Received event {@message}", message);
        }
    }
}
