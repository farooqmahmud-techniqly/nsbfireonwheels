
using Autofac;
using NServiceBus.Logging;
using NServiceBus.Serilog;
using Serilog;

namespace FireOnWheels.OrderAuditService
{
    using NServiceBus;

    /*
		This class configures this endpoint as a Server. More information about how to configure the NServiceBus host
		can be found here: http://particular.net/articles/the-nservicebus-host
	*/
    public class EndpointConfig : IConfigureThisEndpoint
    {
        public void Customize(BusConfiguration configuration)
        {
            var builder = new ContainerBuilder();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .CreateLogger();

            LogManager.Use<SerilogFactory>();

            builder.Register((c, p) => Log.Logger).SingleInstance();

            var container = builder.Build();

            configuration.UseContainer<AutofacBuilder>(c => c.ExistingLifetimeScope(container));
            configuration.UsePersistence<InMemoryPersistence>();

            var conventions = configuration.Conventions();
            conventions.DefiningCommandsAs(t => t.Namespace == "FireOnWheels.QueueService.Common.Commands");
            conventions.DefiningEventsAs(t => t.Namespace == "FireOnWheels.QueueService.Common.Messages");
        }
    }
}
