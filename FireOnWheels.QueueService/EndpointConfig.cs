using Autofac;
using NServiceBus;
using NServiceBus.Logging;
using NServiceBus.Serilog;
using Serilog;

namespace FireOnWheels.QueueService
{
    public class EndpointConfig : IConfigureThisEndpoint
    {
        public void Customize(BusConfiguration configuration)
        {
            var builder = new ContainerBuilder();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .CreateLogger();

            LogManager.Use<SerilogFactory>();

            builder.Register((c, p) => Log.Logger).SingleInstance();

            var container = builder.Build();

            configuration.UseContainer<AutofacBuilder>(c => c.ExistingLifetimeScope(container));
            configuration.UsePersistence<InMemoryPersistence>();

            var conventions = configuration.Conventions();
            conventions.DefiningCommandsAs(t => t.Namespace == "FireOnWheels.QueueService.Common.Commands");
            conventions.DefiningEventsAs(t => t.Namespace == "FireOnWheels.QueueService.Common.Messages");
        }
    }
}