﻿using System;
using FireOnWheels.QueueService.Common.Commands;
using FireOnWheels.QueueService.Common.Messages;
using NServiceBus;
using Serilog;

namespace FireOnWheels.QueueService.Handlers
{
    public class ProcessOrderHandler : IHandleMessages<ProcessOrderCommand>
    {
        private readonly IBus _bus;
        private readonly ILogger _logger;

        public ProcessOrderHandler(IBus bus, ILogger logger)
        {
            _bus = bus;
            _logger = logger.ForContext(GetType());
        }

        public void Handle(ProcessOrderCommand message)
        {
            _logger.Debug("Received message {@message}", message);

            _bus.Publish<IOrderProcessedEvent>(e =>
            {
                e.Id = message.Id;
                e.Description = message.Description;
                e.EventCreatedTime = DateTime.UtcNow;
                e.OrderCreatedTime = message.OrderCreatedTime;
                e.Price = message.Price;
                e.Weight = message.Weight;
                e.SenderAddress = message.SenderAddress;
                e.RecipientAddress = message.RecipientAddress;

                _logger.Debug("Published event {@e}", e);
            });
        }
    }
}