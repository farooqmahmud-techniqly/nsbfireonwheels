﻿using System;

namespace FireOnWheels.QueueService.Common.Commands
{
    public class ProcessOrderCommand
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string SenderAddress { get; set; }
        public string RecipientAddress { get; set; }
        public decimal Weight { get; set; }
        public decimal Price { get; set; }
        public DateTime OrderCreatedTime { get; set; }
    }
}