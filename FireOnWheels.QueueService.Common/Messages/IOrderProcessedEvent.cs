﻿using System;

namespace FireOnWheels.QueueService.Common.Messages
{
    public interface IOrderProcessedEvent
    {
        string Id { get; set; }
        string Description { get; set; }
        string RecipientAddress { get; set; }
        string SenderAddress { get; set; }
        decimal Weight { get; set; }
        decimal Price { get; set; }
        DateTime OrderCreatedTime { get; set; }
        DateTime EventCreatedTime { get; set; }
    }
}