﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using FireOnWheels.Api.Handlers;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace FireOnWheels.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.Remove(config.Formatters.FormUrlEncodedFormatter);

            config.Formatters.JsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            var handler = (RequestLogHandler)config.DependencyResolver.GetService(typeof(RequestLogHandler));
            config.MessageHandlers.Insert(0, handler);
            config.MapHttpAttributeRoutes();

            config.Services.Replace(typeof(IExceptionHandler), config.DependencyResolver.GetService(typeof(ApiExceptionHandler)));
            
        }
    }
}
