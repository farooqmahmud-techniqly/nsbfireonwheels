﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using FireOnWheels.Api.Handlers;
using NServiceBus;
using NServiceBus.Features;
using NServiceBus.Logging;
using NServiceBus.Serilog;
using Serilog;
using SerilogWeb.Classic.Enrichers;

namespace FireOnWheels.Api.App_Start
{
    public class DependencyConfig
    {
        public static void Configure(HttpConfiguration httpConfiguration)
        {
            var builder = new ContainerBuilder();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .Enrich.WithProcessId()
                .Enrich.WithThreadId()
                .Enrich.With<HttpRequestIdEnricher>()
                .Enrich.FromLogContext()
                .CreateLogger();

            LogManager.Use<SerilogFactory>();

            builder.Register((c, p) => Log.Logger).SingleInstance();

            builder.RegisterType<RequestLogHandler>()
                .AsSelf()
                .SingleInstance();

            builder.RegisterType<ApiExceptionHandler>()
                .AsSelf()
                .SingleInstance();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var configuration = new BusConfiguration();
            configuration.DisableFeature<SecondLevelRetries>();
            configuration.DisableFeature<Sagas>();
            configuration.DisableFeature<TimeoutManager>();

            var conventions = configuration.Conventions();
            conventions.DefiningCommandsAs(t => t.Namespace == "FireOnWheels.QueueService.Common.Commands");
            conventions.DefiningEventsAs(t => t.Namespace == "FireOnWheels.QueueService.Common.Messages");

            var container = builder.Build();
            configuration.UseContainer<AutofacBuilder>(c => c.ExistingLifetimeScope(container));

            Bus.CreateSendOnly(configuration);

            httpConfiguration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
