﻿using System;
using System.IO;
using System.Reflection;
using Serilog;
using Serilog.Configuration;
using Serilog.Events;
using Serilog.Formatting.Json;
using Serilog.Sinks.RollingFile.Extension.Sinks;

namespace FireOnWheels.Api
{
    public static class SerilogHelper
    {
        private const long DefaultFileSizeLimitInBytes = 1073741824;
        private const int DefaultRetainedFileDurationLimitInDays = 31;

        public static LoggerConfiguration JsonRollingFile(this LoggerSinkConfiguration sinkConfiguration, string pathFormat, LogEventLevel restrictedToMinimumLevel = LogEventLevel.Verbose, long? fileSizeLimitBytes = null, TimeSpan? retainedFileDurationLimit = null)
        {
            // if pathFormat specifies no directory information, add current AppDomain BaseDirectory
            if (string.IsNullOrWhiteSpace(Path.GetDirectoryName(pathFormat)))
                pathFormat = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? ".", pathFormat);

            return sinkConfiguration.Sink(new SizeRollingFileSink(pathFormat, new JsonFormatter(), fileSizeLimitBytes ?? DefaultFileSizeLimitInBytes, retainedFileDurationLimit ?? TimeSpan.FromDays(DefaultRetainedFileDurationLimitInDays)), restrictedToMinimumLevel);
        }

        public static string DefaultLogPathFormat => $"{Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? ".", Assembly.GetExecutingAssembly().GetName().Name)}-{{Date}}.log";
    }
}
