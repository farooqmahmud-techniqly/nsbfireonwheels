﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;
using Serilog;

namespace FireOnWheels.Api.Handlers
{
    public class ApiExceptionHandler : ExceptionHandler
    {
        private readonly ILogger _logger;

        public ApiExceptionHandler(ILogger logger)
        {
            _logger = logger;
        }

        public override void Handle(ExceptionHandlerContext context)
        {
            context.Result = new ResponseMessageResult(new HttpResponseMessage(HttpStatusCode.InternalServerError));

            _logger.Error(
                context.Exception,
                "Exception On API Request on URL: {RequestUrl}\n Headers: {$RequestHeaders}\n Body: {RequestContent}",
                context.Request.RequestUri.AbsoluteUri,
                context.Request.Headers,
                context.Request.Content != null ? context.Request.Content.ReadAsStringAsync().Result : string.Empty);
        }
    }
}
