﻿using System;

namespace FireOnWheels.Api.Models
{
    public class GetOrder
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public DateTime OrderCreatedTime { get; set; }
        public decimal Price { get; set; }
        public decimal Weight { get; set; }
        public string SenderAddress { get; set; }
        public string RecipientAddress { get; set; }
    }
}