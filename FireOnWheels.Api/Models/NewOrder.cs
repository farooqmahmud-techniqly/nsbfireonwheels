﻿namespace FireOnWheels.Api.Models
{
    public class NewOrder
    {
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string RecipientAddress { get; set; }
        public string SenderAddress { get; set; }
        public decimal Weight { get; set; }
    }
}