﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using FireOnWheels.Api.Models;
using FireOnWheels.QueueService.Common.Commands;
using NServiceBus;

namespace FireOnWheels.Api.Controllers
{
    [RoutePrefix("api/orders")]
    public class OrderController : ApiController
    {
        private readonly ISendOnlyBus _bus;

        public OrderController(ISendOnlyBus bus)
        {
            _bus = bus;
        }

        [HttpPost]
        [Route("", Name = "CreateOrder")]
        public async Task<IHttpActionResult> Create([FromBody] NewOrder newOrder)
        {
            var order = new GetOrder
            {
                Id = Guid.NewGuid().ToString("N"),
                Description = newOrder.Description
            };

            _bus.Send(new ProcessOrderCommand
            {
                Id = order.Id,
                Description = order.Description,
                Price = newOrder.Price,
                RecipientAddress = newOrder.RecipientAddress,
                SenderAddress = newOrder.SenderAddress,
                Weight = newOrder.Weight,
                OrderCreatedTime = DateTime.UtcNow
            });

            return CreatedAtRoute("GetOrder", new {orderId = order.Id}, order);
        }

        [HttpGet]
        [Route("{orderId}", Name = "GetOrder")]
        public async Task<IHttpActionResult> Get([FromUri] string orderId)
        {
            return Ok(new GetOrder {Id = orderId});
        }
    }
}